/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conexion;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Emilio
 */
public class HallarId {

    Conec conec = new Conec();
    Connection conn = conec.conexión();

    public int idArticulo() {
        int id = 1;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            ps = conn.prepareStatement("SELECT MAX(idArticulo) FROM Articulo");
            rs = ps.executeQuery();
            while (rs.next()) {
                id = rs.getInt(1) + 1;
            }
        } catch (SQLException ex) {
            Logger.getLogger(HallarId.class.getName()).log(Level.SEVERE, null, ex);
        }
        /*finally {
            try{
                ps.close();
                rs.close();
                conn.close();
            }catch(Exception ex) {
                JOptionPane.showMessageDialog(null, "Error");
            }
        }*/
        return id;
    }

    public int idDocumento() {
        int id = 1;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            ps = conn.prepareStatement("SELECT MAX(idDocumento) FROM Documento");
            rs = ps.executeQuery();
            while (rs.next()) {
                id = rs.getInt(1) + 1;
            }
        } catch (SQLException ex) {
            Logger.getLogger(HallarId.class.getName()).log(Level.SEVERE, null, ex);
        } 
        return id;
    }

    public int idCliente() {
        int id = 1;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = conn.prepareStatement("SELECT MAX(idCliente) FROM Cliente");
            rs = ps.executeQuery();
            while (rs.next()) {
                id = rs.getInt(1) + 1;
            }
        } catch (SQLException ex) {
            Logger.getLogger(HallarId.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                ps.close();
                rs.close();
                conn.close();
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, "Error");
            }
        }
        return id;
    }
}
