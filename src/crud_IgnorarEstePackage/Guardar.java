/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crud_IgnorarEstePackage;

import conexion.Conec;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import ventanas.Interfaz;

/**
 *
 * @author Emilio
 */
public class Guardar {

    //int idDoc;
    Interfaz inter = new Interfaz();
    
    public void GuardaDB() {
        Conec conec = new Conec();
        Connection conn = conec.conexión();
        
        //, descripcionIngles, idUnidadMedidaCompra, idUnidadMedidaVenta, cantidadUnidadMedidaCompra, precioVentaMayor, precioVentaMenor, precioCompra, stock
        try {
            String sql = "INSERT INTO articulo (idArticulo, descripcion ) VALUES (?,?)";
            
            PreparedStatement pst = conn.prepareStatement(sql);
            pst.setInt(1, Interfaz.idA);
            pst.setString(2, Interfaz.txtArt.getText());
            /*pst.setString(3, null);
            pst.setString(4, null);
            pst.setString(5, null);
            pst.setString(6, null);
            pst.setString(7, null);
            pst.setString(8, null);
            pst.setString(9, null);*/
            
            int r = pst.executeUpdate();
            if (r > 0) {
                JOptionPane.showMessageDialog(null, "Guardado corectamente");
                limpiar();
            }
        } catch (SQLException ex) {
            Logger.getLogger(Interfaz.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        try {
            String sql = "INSERT INTO documento (idDocumento, descripcion, siglas) VALUES (?,?,?)";

            PreparedStatement pst = conn.prepareStatement(sql);
            pst.setInt(1, Interfaz.idD);
            pst.setString(2, Interfaz.txtDes.getText());
            pst.setString(3, "sgl");
            int r = pst.executeUpdate();
            if (r > 0) {
                JOptionPane.showMessageDialog(null, "Guardado corectamente");
                limpiar();
            }
        } catch (SQLException ex) {
            Logger.getLogger(Interfaz.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        try {
            String sql = "INSERT INTO cliente (idCliente, razonSocial, tipoPersona, nroDocumento) VALUES (?,?,?,?)";

            PreparedStatement pst = conn.prepareStatement(sql);
            pst.setInt(1, Interfaz.idC);
            pst.setString(2, Interfaz.txtRaz.getText());
            pst.setString(3, Interfaz.comboTipo.getSelectedItem().toString());
            pst.setString(4, Interfaz.txtNroDoc.getText());
            int r = pst.executeUpdate();
            if (r > 0) {
                JOptionPane.showMessageDialog(null, "Guardado corectamente");
                limpiar();
            }
        } catch (SQLException ex) {
            Logger.getLogger(Interfaz.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    public void limpiar() {
        Interfaz.txtArt.setText("");
        Interfaz.txtDes.setText("");
        Interfaz.txtNroDoc.setText("");
        Interfaz.txtRaz.setText("");

    }
}
